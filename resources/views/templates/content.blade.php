<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    @hasSection ('content-header')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            @yield('content-header')
        </div>
    </div>
    @endif   

    @hasSection ('content-body')
        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">   
                @yield('content-body')
            </div>
        </div>
    @endif
    
</div>   