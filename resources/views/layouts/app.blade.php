<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>AdminLTE 3</title>

        <!-- Font Awesome Icons -->
        <link rel="stylesheet" href="{{ url('plugins/admin-lte/plugins/fontawesome-free/css/all.min.css') }}">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{ url('plugins/admin-lte/dist/css/adminlte.min.css') }}">
        <!-- Google Font: Source Sans Pro -->
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
        <style>
            .bottom-line {
                border-bottom: 1px solid #4f5962;
            }
            .no-padding {
                padding: 0 !important;
            }
            .no-margin {                
                margin: 0 !important;
            }
        </style>
        @yield('css')
    </head>
    <body class="sidebar-mini">
        <div class="wrapper">
            @include('templates.navbar')            
            @include('templates.sidebar')
            @include('templates.content')        
            @hasSection ('control-sidebar')
                @include('templates.sidecontroller')
            @endif        
            @include('templates.footer')
        </div>
        <!-- ./wrapper -->
        
        <!-- REQUIRED SCRIPTS -->
        
        <!-- jQuery -->
        <script src="{{ url('plugins/admin-lte/plugins/jquery/jquery.min.js') }}"></script>
        <!-- Bootstrap 4 -->
        <script src="{{ url('plugins/admin-lte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
        <!-- AdminLTE App -->
        <script src="{{ url('plugins/admin-lte/dist/js/adminlte.min.js') }}"></script>        
        @yield('javascript')
    </body>
</html>