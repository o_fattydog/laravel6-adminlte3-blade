# Laravel 6 with AdminLte 3 Simple Blade Template

## Installation

Use the package manager [composer](https://getcomposer.org/) to install project.

```bash
composer install
```

## Usage

```php
{{-- resources/views/pages/home.blade.php --}}

@extends('layouts.app')

@section('content-header')
    <h1>Home</h1>
@endsection

@section('content-body')
    <p>Welcome to Page.</p>
@endsection

@section('control-sidebar')
    <p>Welcome to this beautiful admin panel.</p>
@endsection

@section('css')
    <style></style>
@endsection

@section('javascript')
    <script> console.log('Hi!'); </script>
@endsection
```

## License

[MIT](https://choosealicense.com/licenses/mit/)
[AdminLTE.io](https://adminlte.io/)